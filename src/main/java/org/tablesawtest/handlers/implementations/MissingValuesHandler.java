package org.tablesawtest.handlers.implementations;

import lombok.Data;
import tech.tablesaw.api.*;
import java.util.Comparator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
public class MissingValuesHandler {
    private Table table;
    public MissingValuesHandler(Table table) {
        this.table = table;
    }

    public Table handle() {
        handleNumeric();
        handleString();
        handleBoolean();
        return table;
    }

    public void handleNumeric() {
        for (NumericColumn<?> column : table.numberColumns()) {
            handleNumericColumn(column.name(), column.type());
        }
    }

    public void handleString() {
        for (StringColumn stringColumn : table.stringColumns()) {
            StringColumn replaceColumn = (StringColumn) stringColumn.interpolate().backfill();
            String name = stringColumn.name();
            replaceColumn.setName(name);
            table.replaceColumn(name, replaceColumn);
            replaceColumn.setMissingTo(commonStringValue(replaceColumn.copy().removeMissing()));
        }
    }

    public void handleBoolean() {
        for (BooleanColumn column : table.booleanColumns()) {
            BooleanColumn replace = (BooleanColumn) column.interpolate().frontfill();
            replace.setName(column.name());
            table.replaceColumn(column.name(), replace);

            if (replace.countFalse() > replace.countTrue()) {
                replace.setMissingTo(false);
            } else {
                replace.setMissingTo(true);
            }
        }
    }

    private String commonStringValue(StringColumn column) {
         return Stream.of(column.asObjectArray())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .max(Comparator.comparingLong(Map.Entry::getValue))
                .map(Map.Entry::getKey).orElse(column.copy().removeMissing().unique().get(0));
    }

    private void handleNumericColumn(String columnName, ColumnType columnType) {
        if (ColumnType.DOUBLE.equals(columnType)) {
            DoubleColumn current = table.doubleColumn(columnName);
            DoubleColumn replace = (DoubleColumn) current.interpolate().frontfill();
            replace.setName(columnName);
            table.replaceColumn(columnName, replace);
            replace.set(replace.isMissing(), replace.median());
        } else if (ColumnType.INTEGER.equals(columnType)) {
            IntColumn current = table.intColumn(columnName);
            IntColumn replace = (IntColumn) current.interpolate().frontfill();
            replace.setName(columnName);
            table.replaceColumn(columnName, replace);
            replace.set(replace.isMissing(), (int) replace.median());
        } else if (ColumnType.LONG.equals(columnType)) {
            LongColumn current = table.longColumn(columnName);
            LongColumn replace = (LongColumn) current.interpolate().frontfill();
            replace.setName(columnName);
            table.replaceColumn(columnName, replace);
            replace.set(replace.isMissing(), (long) replace.median());
        } else {
            throw new IllegalArgumentException("Wrong column type for this numeric handler method");
        }
    }
}
