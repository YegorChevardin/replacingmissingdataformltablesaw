package org.tablesawtest.handlers.implementations;

import tech.tablesaw.api.*;

public class SkewedDataHandler {
    private final Table table;

    public SkewedDataHandler(Table table) {
        this.table = table;
    }

    public Table handle() {
        for (NumericColumn<?> column : table.numberColumns()) {
            handleSkewnessColumn(column.name());
        }

        return table;
    }

    private void handleSkewnessColumn(String columnName) {
        try {
            NumericColumn<?> current = table.numberColumn(columnName);
            resolveSkewness(current);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    private void resolveSkewness(NumericColumn<?> column) {
        double skewness = column.skewness();
        int count = 1;
        String name = column.name();
        NumericColumn<?> fixed = column.copy();

        while ((skewness <= -0.5 || skewness >= 0.5) && count >= 0) {
            fixed = fixed.cubeRoot();
            skewness = fixed.skewness();
            if (Double.isNaN(skewness)) {
                break;
            }
            column = fixed;
            --count;
        }
        column.setName(name);
        table.replaceColumn(name, column);
    }

    public void printSkewness() {
        NumericColumn<?>[] columns = table.numberColumns();

        for (int i = 0; i < columns.length; i++) {
            double columnSkewness = columns[i].skewness();
            String columnName = columns[i].name();
            System.out.println(i + ". Skewness of " + columnName + " is: " + columnSkewness);
        }
    }
}
