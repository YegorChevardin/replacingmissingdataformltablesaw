package org.tablesawtest;

import org.tablesawtest.handlers.implementations.MissingValuesHandler;
import org.tablesawtest.handlers.implementations.SkewedDataHandler;
import tech.tablesaw.api.*;
import tech.tablesaw.columns.Column;

import java.io.*;
import java.util.function.Consumer;

public class App {
    private static final String[] CSV_FILE_NAME = {
            "crash-data-monroe-county-2003-to-2015-1.csv",
            "crash-data-monroe-county-2013-2018-7.csv",
            "crash-data-monroe-county-2019-3.csv",
            "pedestrian-and-bicyclist-counts-2017-8.csv",
            "pedestrian-and-bicyclist-counts-2018-6.csv",
            "pedestrian-and-bicyclist-counts-2019-5.csv",
            "pedestrian-and-bicyclist-counts-2020-1-2020-08-4.csv",
            "traffic-count-1992-1-2019-12-9.csv"
    };

    public static void main(String[] args) {
        int count = 0;
        for (String filePaths : CSV_FILE_NAME) {
            InputStream inputFS = App.class.getClassLoader().getResourceAsStream(filePaths);
            Table beginTable = Table.read().csv(inputFS);
            MissingValuesHandler handler = new MissingValuesHandler(beginTable);
            SkewedDataHandler skewedDataHandler = new SkewedDataHandler(beginTable);

            handler.handle();
            skewedDataHandler.handle();

            for (NumericColumn<?> column : beginTable.numberColumns()) {
                if (column.skewness() >= 0.5 || column.skewness() <= -0.5) {
                    ++count;
                }
                System.out.println("AMOUNT OF MISSING" + " " + column.name() + " " +column.type().name() + " " +filePaths + ": " + column.countMissing());
            }
            save(beginTable, "src/main/resources/completed/" + filePaths);
        }
        System.out.println("AMOUNT OF DEFECTED COLUMNS: " + count);
        //skewedDataHandler.printSkewness();
        //System.out.println("-------------------");
        //skewedDataHandler.handle(beginTable.numberColumns());
        //skewedDataHandler.printSkewness();
        //System.out.println(beginTable.column("Interchange").print());
    }

    public static void save(Table table, String path) {
        try (FileOutputStream outputStream = new FileOutputStream(path)) {
            table.write().csv(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
